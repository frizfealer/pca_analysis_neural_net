function [ synData, preds, labels ] = synthetic_data( m, n, gtWeight, noiseLevel )
%synthetic_data generating data that has part of the feature correlated
%with the label
%input:
%   m: size of data
%   n: size of feature
%   weight for each feature when generating the output, label
%   noiseLevel, noise scale added on the prediction
assert( size(gtWeight, 2) == 1 );
assert( length(gtWeight) == n );
synData = randn( m,n )*1;
preds = sigmoid( synData(:, :)*(gtWeight+randn(n,1)*0.0) + randn(m, 1)*noiseLevel );
labels = zeros(m, 1);
for i = 1:m
    labels(i) = binornd(1, preds(i));
end
% binaryVec = randi(2,length(idx1),1);
% noisePreds(idx1(binaryVec==1)) = preds(idx1(binaryVec==1));
% noisePreds(idx1(binaryVec==2)) = rand(length(find(binaryVec==2)), 1)*0.4 + 0.40;
% 
% binaryVec = randi(2,length(idx2),1);
% noisePreds(idx2(binaryVec==1)) = preds(idx2(binaryVec==1));
% noisePreds(idx2(binaryVec==2)) = 0.8 - rand(length(find(binaryVec==2)), 1)*0.4;


% labels( idx1(randperm(length(idx1), length(idx1))) )=1;
% labels( idx2(randperm(length(idx2), length(idx2))) )=0;
idx = randperm(size(synData, 1), size(synData, 1));
synData = synData(idx, :);
preds = preds(idx, :);
labels = labels(idx, :);
end

