%% first experiment

%% generates synthetic data
rng(2017);
m = 100;
n = 8;
trainPercentage = 0.8;
gtWeight = [1, -1, 1, -1 0 0 0 0]';
%gtWeight = [1 -1 1, -1, 1, -1, 1, -1, 0 0 0 0, 0 ,0 ,0, 0]';

noiseLevel =0.3;
[ synData, preds, labels ] = synthetic_data( m, n, gtWeight, noiseLevel );
trainNum = round( trainPercentage*m );
testNum = m - trainNum;
train.data = synData(1:trainNum, :);
train.preds = preds(1:trainNum);
train.labels = labels(1:trainNum);
test.data = synData(trainNum+1:end, :);
test.preds = preds(trainNum+1:end);
test.labels = labels(trainNum+1:end);

figure;
subplot(1,2,1); hist(preds(labels==0)); title('label 0 dist.');
subplot(1,2,2); hist(preds(labels==1)); title('label 1 dist.');
tabulate(labels)

%% training a logistic regression model
train.X = [ones(trainNum, 1), train.data]; 
test.X = [ones(testNum, 1) test.data];

l2W = 0;
pcaStruc = [];
theta = rand(n+1,1)*0.001;
objFn = @(theta) logistic_regression(theta, train.X, train.labels, l2W, pcaStruc);
tic;
[xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, theta, 100, 1e-3);
fprintf('Optimization took %f seconds.\n', toc);

theta = xVec(:, end);
predProbs = sigmoid(test.X*theta);
predsLabels = zeros(testNum, 1);
predsLabels(predProbs>0.5)=1;
testAcc = mean(predsLabels==test.labels);

predProbs = sigmoid(train.X*theta);
predsLabels = zeros(trainNum, 1);
predsLabels(predProbs>0.5)=1;
trainAcc = mean(predsLabels==train.labels);

fprintf( 'accuracy on training set = %f, test set = %f\n', trainAcc, testAcc );
gradientAry = gradVec';
thetaAry = xVec';
%% pca on the gradientAry
%[coeff,score,latent,tsquared,explained,mu]  = pca(gradientAry);
meanGAry = mean(gradientAry(1:end, :), 1);
lastGAry = gradientAry(end, :);
tmp = bsxfun( @minus, gradientAry(1:end,:), meanGAry );
[U,S,V] = svd(tmp);
eVec = diag( S.^2 / (size(gradientAry(1:end,:), 1) - 1) );
eVec = eVec / sum(eVec);
accuEVec = cumsum(eVec);
figure;
subplot(2,2,1); imagesc(thetaAry); title('weight at each iteration'); ylabel('# iteration'); xlabel('weight'); colorbar; colormap(gca, bluewhitered);
subplot(2,2,2); imagesc(gradientAry); title('gradient at each iteration'); ylabel('# iteration'); xlabel('gradient'); colorbar; colormap(gca, bluewhitered);
subplot(2,2,3); bar(eVec); title('variance explained'); xlabel('principal component');
x = 1:size(V,2);
for i1=1:numel(eVec)
    text(x(i1),eVec(i1),num2str(eVec(i1),'%0.2e'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end
subplot(2,2,4); imagesc(V); colormap(gca, bluewhitered); colorbar; title('principal component coeff.' ); xlabel('principal component');
%% training a logistic regression model with gradient projection on different number of pc components
gradientAry2 = cell(2, 1);
thetaAry2 = cell(2,1);
for z = 1:2
    tMat = V(:,1:z);
    pcaStruc.tMat = tMat;
    pcaStruc.meanGAry = lastGAry;
    train.X = [ones(trainNum, 1), train.data]; 
    test.X = [ones(testNum, 1) test.data];

    l2W = 0;
    theta = rand(n+1,1)*0.001;
    objFn = @(theta) logistic_regression(theta, train.X, train.labels, l2W, pcaStruc);
    tic;
    [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, theta, 100, 1e-3);
    fprintf('Optimization took %f seconds.\n', toc);

    theta = xVec(:, end);
    predProbs = sigmoid(test.X*theta);
    predsLabels = zeros(testNum, 1);
    predsLabels(predProbs>0.5)=1;
    testAcc = mean(predsLabels==test.labels);

    predProbs = sigmoid(train.X*theta);
    predsLabels = zeros(trainNum, 1);
    predsLabels(predProbs>0.5)=1;
    trainAcc = mean(predsLabels==train.labels);
    fprintf( 'accuracy on training set = %f, test set = %f\n', trainAcc, testAcc );
    idx = find(fVec(2:end)-fVec(1:end-1)>0, 1);
    if ~isempty(idx)
        gradientAry2{z} = gradVec(:, 1:idx)';
        thetaAry2{z} = xVec(:, 1:idx)';
    else
        gradientAry2{z} = gradVec';
        thetaAry2{z} = xVec';
    end
end
%% results
figure;
titleTxt{1} = '1';
titleTxt{2} = '1~2';
for i = 1:2
    subplot(2, 2, i); imagesc(gradientAry2{i}); colormap(gca, bluewhitered); colorbar; title( sprintf( 'gradients w. pca on the %s components', titleTxt{i} )); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 2, i+2); imagesc(thetaAry2{i}); colormap(gca, bluewhitered); colorbar; title( sprintf( 'theta w. pca on the %s components', titleTxt{i} ) ); ylabel('# iterations');  xlabel('weight');
end