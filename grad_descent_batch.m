function [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, x0, maxiter, stepSize, dataAry, labelsVec, batch_size, decrease_lr_it)
%grad_descent_batch gradient descent for mini-batch
%input
%   objFn: objective function handle
%   x0: starting point of parameters
%   maxiter: maximun iteration for optimization
%   stepSize: step size for each update
%   dataAry: input data array
%   labelsVec: input data label
%   batch_size: the size of mini-batch
%   decrease_lr_it: the iteration number after which the learning rate
%   could be decreased by half.


% termination tolerance
tol = 1e-6;

% maximum number of allowed iterations
%maxiter = 100;

% minimum allowed perturbation
dxmin = 1e-6;

if isempty(decrease_lr_it)
    decrease_lr_it = 500;
end

% step size ( 0.33 causes instability, 0.2 quite accurate)
alpha = stepSize;

% initialize gradient norm, optimization vector, iteration counter, perturbation
gnorm = inf; x = x0; niter = 1; dx = inf;
xVec = zeros(length(x0), maxiter+1);
gradVec = zeros(length(x0), maxiter+1);
fVec = zeros(1, maxiter+1);
% gradient descent algorithm:
tolCnt = 0;
randIdx = randsample(size(dataAry, 1), batch_size);
cData = dataAry( randIdx, : );
cLabel = labelsVec( randIdx );
[f, g] = objFn(x, cData, cLabel);
fVec(1) = f;
gradVec(:, 1) = g;
xVec(:, 1) = x;
niter = 2;
gnorm = norm(g);
alpha = stepSize;

while and(gnorm>=tol, and(niter <= maxiter+1, dx >= dxmin))
    if niter-1 > decrease_lr_it && mod(niter-1, 100) == 0
        alpha = alpha / 2;
        fprintf('new alpha = %f\n', alpha );
    end
    %test a step
    xtest= x - alpha*g;
    [ftest, gtest] = objFn(xtest, cData, cLabel);
%     if ftest > f
%         tolCnt = tolCnt + 1;
%     else
%         tolCnt = 0;
%     end
%     if tolCnt > 10
%         break
%     end
%     while ftest > f
%         alpha = alpha*0.8;
%         xtest= x - alpha*g;
%         [ftest, gtest] = objFn(xtest, cData, cLabel);   
%         fprintf( 'niter = %d, updated alpha = %f\n', niter, alpha);
%     end
    x = xtest;
    f = ftest;
    g = gtest;
    gnorm = norm(g);
%     if ftest > f
%         break
%     end
    % take step:
    % check step
    if ~isfinite(x)
        display(['Number of iterations: ' num2str(niter)])
        error('x is inf or NaN')
    end
    % update termination metrics
    fVec(niter) = f;
    gradVec(:, niter) = g;
    xVec(:, niter) = x;
    dx = norm(x-xVec(:, niter-1));    
    niter = niter + 1;
    
    randIdx = randsample(size(dataAry, 1), batch_size);
    cData = dataAry( randIdx, : );
    cLabel = labelsVec( randIdx );     
end
niter = niter - 1;
fVec = fVec(1:niter);
xVec = xVec(:,1:niter);
gradVec = gradVec(:, 1:niter);
end