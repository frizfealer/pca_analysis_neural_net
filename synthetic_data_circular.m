function [ synData, labels ] = synthetic_data_circular( m, minDistTimes, maxDistTimes )
%synthetic_data_circular generate the circular dataset
%input
%   m: the size of data
%   minDistTimes: the smaller radius of the outside circle
%   maxDistTimes: the larger radius of the outside circle
assert(minDistTimes < maxDistTimes);
g1Num = round(m/2);
g2Num = m - g1Num;
grp1 = zeros(g1Num, 2);

enoughFlag = 0;
cnt = 1;
while enoughFlag == 0
    tmp = (rand(g1Num*10, 2)-0.5)*2;
    insVec = sqrt(tmp(:,1).^2+tmp(:,2).^2);
    insVec = find(insVec <= 1);
    endIdx = cnt+length(insVec)-1;
    if endIdx > g1Num
        endIdx = g1Num;
        enoughFlag = 1;
    end
    grp1(cnt:endIdx, :) = tmp(insVec(1:(endIdx-cnt+1)), :);
end

enoughFlag = 0;
cnt = 1;
while enoughFlag == 0
    tmp = (rand(g2Num*10, 2)-0.5)*2*(maxDistTimes);
    insVec = sqrt(tmp(:,1).^2+tmp(:,2).^2);
    insVec = find(insVec <= maxDistTimes & insVec >= minDistTimes);
    endIdx = cnt+length(insVec)-1;
    if endIdx > g1Num
        endIdx = g1Num;
        enoughFlag = 1;
    end
    grp2(cnt:endIdx, :) = tmp(insVec(1:(endIdx-cnt+1)), :);
end

synData = [grp1; grp2];
labels = zeros(m, 1);
labels(g1Num+1:end) = 1;

idx = randperm(size(synData, 1), size(synData, 1));
synData = synData(idx, :);
labels = labels(idx, :);

end