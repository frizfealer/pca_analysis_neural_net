function [ xVec, fVec, gradVec, niter] = optimizeWrapper( ei, costFn, pcaStruc )
rng(2018);
% setup random initial weights
stack = initialize_weights(ei);
params = stack2params(stack);
% run training
objFn = @(params) costFn(params, ei, train.data, train.labels, 0, 0, pcaStruc);
maxiter = 1000;
stepSize = 5e-3;
[xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, params, maxiter, stepSize);


end

