function [ VX, algoName, xAryPCCell ] = optimizationPathPlot( thetaCell, algoName, costFn, xLim, yLim, VX, labels, showIdx )
%optimizationPathPlot a function generates two plots. One with the contour
%of accuracy, another with the contour of objective function. Either plots
%has the same optimzation paths from different projection algorithm.
%input:
%   thetaCell: a cell structure. Each contains the weight changing across
%   iterations for an algorithm.
%   algoName: the respected algorithm name shown in the legend.
%   costFn: the handle of the objective function.
%   xLim, yLim, the x-axis and y-axis limits of the plots, could be []
%   VX: the eigen vector of all the weights, could be []
%   labels: the label used in costFn, usefull when generating accuracy
%   contour.
%   showIdx: the algorithm showns in the final output.
%output:
%   VX: the eigenvectors computing from the weights
%   algoName: the final algoName shown in the figure. Useful when adding 2
%   pc loading in the generated counter plots
%   xAryPCCell: the projected weight of each algorithms. Each cell contains
%   an the weights for an algorithm. 
allThetaAry = thetaCell{1};
for i = 2:length(thetaCell)
    allThetaAry = [allThetaAry; thetaCell{i}];
end
lastXAry = zeros( size(allThetaAry(end,:)) );
%lastXAry = allThetaAry(end,:);
tmp = bsxfun( @minus, allThetaAry(1:end,:), lastXAry );
if isempty(VX)
    [U,S,VX] = svd(tmp);
    
    eVec = diag( S.^2 / (size(allThetaAry(1:end,:), 1) - 1) );
    eVec = eVec / sum(eVec);
    figure; 
    subplot(2,2,1); imagesc(allThetaAry); colormap(gca, bluewhitered);  title('weights from different algorithms'); ylabel('# iteration'); xlabel('weight'); colorbar;
    subplot(2,2,2); bar(eVec); title('variance explained'); xlabel('principal component');
    x = 1:size(VX,2);
    for i1=1:numel(eVec)
        text(x(i1),eVec(i1),num2str(eVec(i1),'%0.2e'),...
                   'HorizontalAlignment','center',...
                   'VerticalAlignment','bottom')
    end
    subplot(2,2,3); imagesc(VX); colormap(gca, bluewhitered);  colorbar; title('principal component coeff.' ); xlabel('principal component');
end

scores = tmp*VX(:, 1:2);
maxminVal = zeros(2, 2);
for i = 1:2
    maxminVal(1, i) = max(scores(:, i))+2;
    maxminVal(2, i) = min(scores(:, i))-2;
end
if isempty( xLim  )
    x = linspace(maxminVal(2, 1), maxminVal(1, 1));
else
    x = linspace(xLim(1), xLim(2));
end
if isempty( yLim )
    y = linspace(maxminVal(2, 2), maxminVal(1, 2));
else
    y = linspace(yLim(1), yLim(2));
end
[X,Y] = meshgrid(x,y);
Z = bsxfun( @plus, [X(:) Y(:)]*VX(:, 1:2)', lastXAry );
cost = zeros(size(Z, 1), 1);
acc = zeros(size(Z, 1), 1);
for i = 1:size(Z, 1)
    [cost(i),~,tmp] = costFn(Z(i, :));
    predsLabels = zeros(length(labels), 1);
    predsLabels(tmp>0.5)=1;
    acc(i) = mean(predsLabels==labels);    
end
cost = reshape(cost, [100, 100]);
acc = reshape(acc, [100, 100]);
gVec = cell(2,1);
figure;
contour(X, Y, acc, 'ShowText','on', 'levelStep', 0.05); title('optimization path(train acc.)', 'fontweight', 'bold');
gVec{1} = gcf;
xlabel('pc1', 'fontweight', 'bold');
ylabel('pc2', 'fontweight', 'bold');
axis equal

figure;
contour(X,Y,cost, 'ShowText','on', 'levelStep', 20);
gVec{2} = gcf;

xAryPCCell = cell(length(thetaCell), 1);
for i = showIdx
    cThetaAry = thetaCell{i};
    xAry = [cThetaAry];
    xAry = bsxfun( @minus, xAry, lastXAry );
    xAryPC = xAry*VX(:, 1:2);
    xAryVec = xAryPC(2:end, :) - xAryPC(1:end-1, :);
    xAryPCCell{i} = xAryPC;
    figure(gVec{1}); hold on; tmp= quiver( xAryPC(1:end-1, 1), xAryPC(1:end-1, 2), xAryVec(:, 1), xAryVec(:, 2), 'AutoScale', 'off', 'LineWidth', 1.5 );
    figure(gVec{2}); hold on; tmp= quiver( xAryPC(1:end-1, 1), xAryPC(1:end-1, 2), xAryVec(:, 1), xAryVec(:, 2), 'AutoScale', 'off', 'LineWidth', 1.5 );
    %hold on; arrow(xAryPC(1:end-1, :), xAryVec);
end
tmp = algoName;
algoName = cell(length(algoName)+1, 1);
for i = 1:length(algoName)
    if i == 1
        algoName{i} = '';
    else
        algoName{i} = tmp{i-1};
    end
end
algoName = algoName([1 showIdx+1]);
figure(gVec{2});
legend(algoName);
    set(gca, 'fontweight', 'bold' );
    xlabel('pc1');
    ylabel('pc2');
    axis equal;
    title( 'optimization path');
end
