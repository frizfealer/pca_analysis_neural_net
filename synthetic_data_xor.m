function [ synData, labels ] = synthetic_data_xor( m )
%synthetic_data_xor generate xor dataset two features with two labels
%input: 
%   m: size of data
%output:
%   synData: synthetic dataset
%   labels: labels of the synthetic dataset
c1 = randn( m/4,2 ) + 3; %cluster at [1, 1]
c2 = randn( m/4, 2) - 3; %cluster at [-1, -1]
c3 = [(randn( m/4, 1) + 3) (randn(m/4, 1)-3)]; %cluster at[1, -1]
c4 = [(randn( m/4, 1) - 3) (randn(m/4, 1)+3)]; %cluster at [-1 ,1]
synData = [c1; c2; c3; c4];
labels = zeros(m, 1);
labels(1:m/2) = 1;
labels(m/2+1:end) = 0;


idx = randperm(size(synData, 1), size(synData, 1));
synData = synData(idx, :);
%preds = preds(idx, :);
labels = labels(idx, :);
end

