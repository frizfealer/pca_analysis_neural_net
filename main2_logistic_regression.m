%% generates synthetic data

clear all
rng(2017);
m = 1000;
n=2;
trainPercentage = 0.8;

% c1 = randn( m/2,2 ) + 3; %cluster at [1, 1]
% c2 = randn( m/2, 2) - 3; %cluster at [-1, -1]figure;scatter(synData(labels==0, 1), synData(labels==0,2));
% synData = [c1; c2];
% labels = zeros(m, 1);
% labels(1:m/2) = 1;
% labels(m/2+1:end) = 0;

[ synData, labels ] = synthetic_data_xor( m );

figure;scatter(synData(labels==0, 1), synData(labels==0,2));
hold on;
scatter(synData(labels==1, 1), synData(labels==1,2));

trainNum = round( trainPercentage*m );
testNum = m - trainNum;
train.data = synData(1:trainNum, :);
train.labels = labels(1:trainNum);
test.data = synData(trainNum+1:end, :);
test.labels = labels(trainNum+1:end);

%% training the logistic regression with mini-batch
trainX = [ones(trainNum, 1), train.data]; 
testX = [ones(testNum, 1) test.data];

l2W = 0;
pcaStruc = [];
rng(2017);
initTheta = rand(3,1)*0.001;
objFn = @(theta, x, y) logistic_regression(theta, x, y, 0, pcaStruc);
tic;
% [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, theta, 100, 1e-3);
stepSize = 5e-3;
maxiter = 1000;
decarse_lr_it = 100;
batch_size = 100;
[xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, initTheta, maxiter, stepSize, trainX, train.labels, batch_size, decarse_lr_it);
fprintf('Optimization took %f seconds.\n', toc);
model{1}.xVec= xVec;
model{1}.fVec= fVec;
model{1}.gradVec= gradVec;


theta = xVec(:, end);
predProbs = sigmoid(testX*theta);
predsLabels = zeros(testNum, 1);
predsLabels(predProbs>0.5)=1;
testAcc = mean(predsLabels==test.labels);

predProbs = sigmoid(trainX*theta);
predsLabels = zeros(trainNum, 1);
predsLabels(predProbs>0.5)=1;
trainAcc = mean(predsLabels==train.labels);

fprintf( 'accuracy on training set = %f, test set = %f\n', trainAcc, testAcc );
model{1}.testAcc = testAcc;

%visualization
x1dist = -xVec(1, end) / xVec(2, end);
x2dist = -xVec(1, end) / xVec(3, end);
m = (x2dist-0)/(0-x1dist);
n = 0 - m*x1dist;
x1(1) = min(synData(:,1));
x1(2) = x1(1)*m+n;
x2(1) = max(synData(:,1));
x2(2) = x2(1)*m+n;
figure;scatter(synData(labels==0, 1), synData(labels==0,2));
hold on;
scatter(synData(labels==1, 1), synData(labels==1,2));
plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
title('hyperplane');
clear xVec fVec gradVec niter gnorm dx

figure;plot(model{1}.fVec); set(gca, 'fontweight', 'bold'); title('objective function changing');
xlabel('# iteration');

%% pca on the 1 to 400 iterations hundred gradientAry
%[coeff,score,latent,tsquared,explained,mu]  = pca(gradientAry);
gradientAry = model{1}.gradVec(:, 1:400)';
thetaAry = model{1}.xVec(:, 1:400)';
lastGAry = gradientAry(end,:);
zeroGAry = zeros(size(lastGAry));
tmp = bsxfun( @minus, gradientAry(1:end,:), zeroGAry );
[U,S,V] = svd(tmp);
eVec = diag( S.^2 / (size(gradientAry(1:end,:), 1) - 1) );
eVec = eVec / sum(eVec);
accuEVec = cumsum(eVec);
figure; 
subplot(2,2,1); imagesc(thetaAry); colormap(gca, bluewhitered);  title('weight at each iteration'); ylabel('# iteration'); xlabel('weight'); colorbar;
subplot(2,2,2); imagesc(gradientAry);  colormap(gca, bluewhitered);  title('gradient at each iteration'); ylabel('# iteration'); xlabel('gradient'); colorbar;
subplot(2,2,3); bar(eVec); title('variance explained'); xlabel('principal component');
x = 1:size(V,2);
for i1=1:numel(eVec)
    text(x(i1),eVec(i1),num2str(eVec(i1),'%0.2e'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end
subplot(2,2,4); imagesc(V); colormap(gca, bluewhitered);  colorbar; title('principal component coeff.' ); xlabel('principal component');

%%  training a neural net model with first component's projection (batch optimization)
componentUsed = [1, 2, 3];
for i = 1:length(componentUsed)
    clear xVec fVec gradVec niter gnorm dx
    %select the transformed matrix
    tMat = V(:,componentUsed(1:i));
    pcaStruc.tMat = tMat;
    pcaStruc.meanGAry = zeros( size(lastGAry) );
    train.X = [ones(trainNum, 1), train.data]; 
    test.X = [ones(testNum, 1) test.data];

    l2W = 0;
    objFn = @(theta, x, y) logistic_regression(theta, x, y, 0, pcaStruc);
    tic;
    % [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, theta, 100, 1e-3);
    stepSize = 5e-3;
    maxiter = 1000;
    decarse_lr_it = 100;
    batch_size = 100;
    [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, initTheta, maxiter, stepSize, train.X, train.labels, batch_size, decarse_lr_it);
    tmpStr = sprintf( '1~%d', i );
    figure;plot(fVec); set(gca, 'fontweight', 'bold'); title( sprintf( 'objective function changing with pc components %s projection', tmpStr ) );
    xlabel('# iteration');
    model{i+1}.xVec= xVec;
    model{i+1}.fVec= fVec;
    model{i+1}.gradVec= gradVec;
    theta = xVec(:, end);
    predProbs = sigmoid(test.X*theta);
    predsLabels = zeros(testNum, 1);
    predsLabels(predProbs>0.5)=1;
    testAcc = mean(predsLabels==test.labels);

    predProbs = sigmoid(train.X*theta);
    predsLabels = zeros(trainNum, 1);
    predsLabels(predProbs>0.5)=1;
    trainAcc = mean(predsLabels==train.labels);

    fprintf( 'accuracy on training set = %f, test set = %f\n', trainAcc, testAcc );
    
    
    figure;scatter(train.data(train.labels==0, 1), train.data(train.labels==0,2));
    hold on;
    scatter(train.data(train.labels==1, 1),train.data(train.labels==1,2));
    x1dist = -xVec(1, end) / xVec(2, end);
    x2dist = -xVec(1, end) / xVec(3, end);
    m = (x2dist-0)/(0-x1dist);
    n = 0 - m*x1dist;
    x1(1) = min(synData(:,1));
    x1(2) = x1(1)*m+n;
    x2(1) = max(synData(:,1));
    x2(2) = x2(1)*m+n;
    plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
    title( sprintf( 'hyperplane from pc component %s projection', tmpStr ) );
end

%% PCA projection training results
figure;
componentStr1 = {'1^{st}', '1^{st} to 2^{nd}', '1^{st} to 3^{rd}'};
for i = 1:3
    subplot(2, 3, i); imagesc(model{i+1}.gradVec'); colormap(gca, bluewhitered);  colorbar; title(['gradients on the ' componentStr1{i} ' components']); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 3, i+3); imagesc(model{i+1}.xVec'); colormap(gca, bluewhitered);  colorbar; title(['theta on the ' componentStr1{i} ' components']); ylabel('# iterations');  xlabel('weight');
end

%% pca on the 800:1000 iterations hundred gradientAry
%[coeff,score,latent,tsquared,explained,mu]  = pca(gradientAry);
gradientAry = model{1}.gradVec(:, 800:1000)';
thetaAry = model{1}.xVec(:, 800:1000)';
lastGAry = gradientAry(end,:);
zeroGAry = zeros(size(lastGAry));
tmp = bsxfun( @minus, gradientAry(1:end,:), zeroGAry );
[U,S,V] = svd(tmp);
eVec = diag( S.^2 / (size(gradientAry(1:end,:), 1) - 1) );
eVec = eVec / sum(eVec);
accuEVec = cumsum(eVec);
figure; 
subplot(2,2,1); imagesc(thetaAry); colormap(gca, bluewhitered);  title('weight at each iteration'); ylabel('# iteration'); xlabel('weight'); colorbar;
subplot(2,2,2); imagesc(gradientAry);  colormap(gca, bluewhitered);  title('gradient at each iteration'); ylabel('# iteration'); xlabel('gradient'); colorbar;
subplot(2,2,3); bar(eVec); title('variance explained'); xlabel('principal component');
x = 1:size(V,2);
for i1=1:numel(eVec)
    text(x(i1),eVec(i1),num2str(eVec(i1),'%0.2e'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end
subplot(2,2,4); imagesc(V); colormap(gca, bluewhitered);  colorbar; title('principal component coeff.' ); xlabel('principal component');

%%  training a neural net model with first component's projection (batch optimization)
componentUsed = [1, 2, 3];
for i = 1:length(componentUsed)
    clear xVec fVec gradVec niter gnorm dx
    %select the transformed matrix
    tMat = V(:,componentUsed(1:i));
    pcaStruc.tMat = tMat;
    pcaStruc.meanGAry = zeros( size(lastGAry) );
    train.X = [ones(trainNum, 1), train.data]; 
    test.X = [ones(testNum, 1) test.data];

    l2W = 0;
    objFn = @(theta, x, y) logistic_regression(theta, x, y, 0, pcaStruc);
    tic;
    % [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, theta, 100, 1e-3);
    stepSize = 5e-3;
    maxiter = 1000;
    decarse_lr_it = 100;
    batch_size = 100;
    [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, initTheta, maxiter, stepSize, train.X, train.labels, batch_size, decarse_lr_it);
    tmpStr = sprintf( '1~%d', i );
    figure;plot(fVec); set(gca, 'fontweight', 'bold'); title( sprintf( 'objective function changing with pc components %s projection', tmpStr ) );
    model{i+1}.xVec= xVec;
    model{i+1}.fVec= fVec;
    model{i+1}.gradVec= gradVec;
    theta = xVec(:, end);
    predProbs = sigmoid(test.X*theta);
    predsLabels = zeros(testNum, 1);
    predsLabels(predProbs>0.5)=1;
    testAcc = mean(predsLabels==test.labels);

    predProbs = sigmoid(train.X*theta);
    predsLabels = zeros(trainNum, 1);
    predsLabels(predProbs>0.5)=1;
    trainAcc = mean(predsLabels==train.labels);

    fprintf( 'accuracy on training set = %f, test set = %f\n', trainAcc, testAcc );
    model{i+1}.testAcc = testAcc;
    
    figure;scatter(train.data(train.labels==0, 1), train.data(train.labels==0,2));
    hold on;
    scatter(train.data(train.labels==1, 1),train.data(train.labels==1,2));

    x1dist = -xVec(1, end) / xVec(2, end);
    x2dist = -xVec(1, end) / xVec(3, end);
    m = (x2dist-0)/(0-x1dist);
    n = 0 - m*x1dist;
    x1(1) = min(synData(:,1));
    x1(2) = x1(1)*m+n;
    x2(1) = max(synData(:,1));
    x2(2) = x2(1)*m+n;
    plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
    title( sprintf( 'hyperplane from pc component %s projection', tmpStr ) );
end

%% PCA projection training results
figure;
componentStr1 = {'1^{st}', '1^{st} to 2^{nd}', '1^{st} to 3^{rd}'};
for i = 1:3
    subplot(2, 3, i); imagesc(model{i+1}.gradVec'); colormap(gca, bluewhitered);  colorbar; title(['gradients on the ' componentStr1{i} ' components']); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 3, i+3); imagesc(model{i+1}.xVec'); colormap(gca, bluewhitered);  colorbar; title(['theta on the ' componentStr1{i} ' components']); ylabel('# iterations');  xlabel('weight');
end