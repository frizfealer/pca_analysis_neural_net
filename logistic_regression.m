function [f,g] = logistic_regression(theta, X, y, l2W, pcaStruc)
%logistic_regression 
%input:
%   theta: model parameters,
%   X: data array
%   y: label array
%   l2W: l2 regularization on weight
%   pcaStruc: a structure contains two fields
%       a. meanGAry: mean gradient to be subtracted from
%       b. tMat a matrix of size [m, c], where m is # features, and c is
%       # of components
%output:
%   f: function value
%   g: gradient
  m=size(X,2);
  
  % initialize objective value and gradient.
  
  preds = sigmoid( X*theta );
  
  f = -y'*log(preds + 1e-16) - (1-y)'*log(1-preds + 1e-16); %cost
  g = X'*(preds - y);
  
  if l2W ~= 0
      f = f + l2W*0.5*(theta(1:end)'*theta(1:end));
      g = g + l2W*theta(1:end);
  end
  
  if ~isempty(pcaStruc)
      g = g - pcaStruc.meanGAry';
      tmp = (pcaStruc.tMat'*g);
      g = pcaStruc.tMat*tmp;
      g = g+ pcaStruc.meanGAry';
  end
  
end

