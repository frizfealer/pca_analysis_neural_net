%% the second experiment

%% generates synthetic data
clear all
rng(2017);
m = 1000;
n=2;
trainPercentage = 0.8;
[ synData, labels ] = synthetic_data_xor( m );
figure;scatter(synData(labels==0, 1), synData(labels==0,2));
hold on;
scatter(synData(labels==1, 1), synData(labels==1,2));

trainNum = round( trainPercentage*m );
testNum = m - trainNum;
train.data = synData(1:trainNum, :);
train.labels = labels(1:trainNum);
test.data = synData(trainNum+1:end, :);
test.labels = labels(trainNum+1:end);

%% training a nerual network with batch optimize
clear xVec fVec gradVec niter gnorm dx
% ei: a data struce for network
% this is a one hidden layer, two hidden nodes network.
ei.input_dim = 2;
ei.output_dim = 1;
ei.layer_sizes = [2, ei.output_dim];
rng(2018); stack = initialize_weights(ei);
params = stack2params(stack);
pcaStruc = [];
objFn = @(params, x, y) supervised_dnn_cost(params, ei, x, y, 0, 0, pcaStruc);
maxiter = 1000;
stepSize = 5e-3;
[xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, params, maxiter, stepSize, train.data, train.labels, 100, []);
figure;plot(fVec); set(gca, 'fontweight', 'bold'); title('objective function changing'); xlabel('# iteration');
model{1}.xVec= xVec;
model{1}.fVec= fVec;
model{1}.gradVec= gradVec;

% compute accuracy on the test and train set
[~, ~, predProbs] = supervised_dnn_cost( xVec(:, end), ei, train.data, train.labels, 0, true);
predsLabels = zeros(trainNum, 1);
predsLabels(predProbs>0.5)=1;
trainAcc = mean(predsLabels==train.labels);
[~, ~, predProbs, hiddenOut] = supervised_dnn_cost( xVec(:, end), ei, test.data, test.labels, 0, true);
predsLabels = zeros(testNum, 1);
predsLabels(predProbs>0.5)=1;
testAcc = mean(predsLabels==test.labels);
fprintf('train accuracy: %f, test accuracy: %f\n', trainAcc, testAcc);
model{1}.testAcc = testAcc;

figure; pgca(1) = subplot(2, 2, 1); axis equal;
scatter(test.data(test.labels==0, 1), test.data(test.labels==0,2));
hold on;
scatter(test.data(test.labels==1, 1),test.data(test.labels==1,2));
legend('label0', 'label1');
title('ground-truth','fontweight', 'bold');
pgca(2) = subplot(2, 2, 2); axis equal;
scatter(test.data(predsLabels==0, 1), test.data(predsLabels==0,2));
hold on;
scatter(test.data(predsLabels==1, 1),test.data(predsLabels==1,2));
title('prediction','fontweight', 'bold');
weight = reshape(xVec([1:4], end), 2, 2);
bias = xVec([5:6], end);
x1dist = -bias(1) / weight(1, 1);
x2dist = -bias(1) / weight(1, 2);
m = (x2dist-0)/(0-x1dist);
n = 0 - m*x1dist;
x1(1) = min(test.data(:,1));
x1(2) = x1(1)*m+n;
x2(1) = max(test.data(:,1));
x2(2) = x2(1)*m+n;
aaa=plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
xmax = max(x1(1), x2(1));
xmin = min(x1(1), x2(1));
ymax = max(x1(2), x2(2));
ymin = min(x1(2), x2(2));
x1dist = -bias(2) / weight(2, 1);
x2dist = -bias(2) / weight(2, 2);
m = (x2dist-0)/(0-x1dist);
n = 0 - m*x1dist;
x1(1) = min(test.data(:,1));
x1(2) = x1(1)*m+n;
x2(1) = max(test.data(:,1));
x2(2) = x2(1)*m+n;
bbb=plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
legend([aaa,bbb], 'sep1', 'sep2');
xmax = max( max( x1(1), x2(1) ), xmax );
xmin = min( min( x1(1), x2(1) ), xmin );
ymax = max( max( x1(2), x2(2) ), ymax );
ymin = min( min( x1(2), x2(2) ), ymin );
set(pgca(1), 'xlim', [xmin, xmax], 'ylim', [ymin, ymax]);
set(pgca(2), 'xlim', [xmin, xmax], 'ylim', [ymin, ymax]);

tmp = [hiddenOut{1}, predProbs];
subplot(2, 2, 3); imagesc(tmp(predsLabels==0, :));  colorbar; title('subset of label(class) 0', 'fontweight', 'bold'); set(gca, 'xtick', [1, 2, 3], 'fontweight', 'bold'); xlabel( 'node' ); ylabel( 'sample'); 
subplot(2, 2, 4); imagesc(tmp(predsLabels==1, :));  colorbar; title('subset of label(class) 1', 'fontweight', 'bold'); set(gca, 'xtick', [1, 2, 3], 'fontweight', 'bold'); xlabel( 'node' ); ylabel( 'sample');

%%
%% pca on the 1 to 600 iterations hundred gradientAry
%[coeff,score,latent,tsquared,explained,mu]  = pca(gradientAry);
gradientAry = model{1}.gradVec(:, 1:600)';
thetaAry = model{1}.xVec(:, 1:600)';
lastGAry = gradientAry(end,:);
zeroGAry = zeros(size(lastGAry));
tmp = bsxfun( @minus, gradientAry(1:end,:), zeroGAry );
[U,S,V] = svd(tmp);
eVec = diag( S.^2 / (size(gradientAry(1:end,:), 1) - 1) );
eVec = eVec / sum(eVec);
accuEVec = cumsum(eVec);
figure; 
subplot(2,2,1); imagesc(thetaAry); colormap(gca, bluewhitered);  title('weight at each iteration'); ylabel('# iteration'); xlabel('weight'); colorbar;
subplot(2,2,2); imagesc(gradientAry);  colormap(gca, bluewhitered);  title('gradient at each iteration'); ylabel('# iteration'); xlabel('gradient'); colorbar;
subplot(2,2,3); bar(eVec); title('variance explained'); xlabel('principal component');
x = 1:size(V,2);
for i1=1:numel(eVec)
    text(x(i1),eVec(i1),num2str(eVec(i1),'%0.2e'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end
subplot(2,2,4); imagesc(V); colormap(gca, bluewhitered);  colorbar; title('principal component coeff.' ); xlabel('principal component');

%%  training a neural net model with first component's projection (batch optimization)
componentUsed = [1, 2, 3, 4, 5, 6, 7];
for i = 1:length(componentUsed)
    clear xVec fVec gradVec niter gnorm dx
    %select the transformed matrix
    tMat = V(:,componentUsed(1:i));
    pcaStruc.tMat = tMat;
    pcaStruc.meanGAry = zeros( size(lastGAry) );
    train.X = [ones(trainNum, 1), train.data]; 
    test.X = [ones(testNum, 1) test.data];

    rng(2018);
    stack = initialize_weights(ei);
    params = stack2params(stack);
    objFn = @(params, x, y) supervised_dnn_cost(params, ei, x, y, 0, 0, pcaStruc);
    maxiter = 1000;
    stepSize = 5e-3;
    [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, params, maxiter, stepSize, train.data, train.labels, 100, []);
    tmpStr = sprintf( '1 ~ %d', i );
    figure;plot(fVec); set(gca, 'fontweight', 'bold'); title( sprintf( 'objective function changing using pc component %s to project', tmpStr) ); xlabel('# iteration');
    model{i+1}.xVec= xVec;
    model{i+1}.fVec= fVec;
    model{i+1}.gradVec= gradVec;
    % compute accuracy on the test and train set
    [~, ~, predProbs] = supervised_dnn_cost( xVec(:, end), ei, train.data, train.labels, 0, true, pcaStruc);
    predsLabels = zeros(trainNum, 1);
    predsLabels(predProbs>0.5)=1;
    trainAcc = mean(predsLabels==train.labels);
    [~, ~, predProbs] = supervised_dnn_cost( xVec(:, end), ei, test.data, test.labels, 0, true, pcaStruc);
    predsLabels = zeros(testNum, 1);
    predsLabels(predProbs>0.5)=1;
    testAcc = mean(predsLabels==test.labels);
    fprintf('train accuracy: %f, test accuracy: %f\n', trainAcc, testAcc);

    figure;scatter(train.data(train.labels==0, 1), train.data(train.labels==0,2));
    hold on;
    scatter(train.data(train.labels==1, 1),train.data(train.labels==1,2));
    set(gca, 'fontweight', 'bold'); title( sprintf( 'the hyperplanes from the hidden layer using pc component %s to project', tmpStr ) )

    weight = reshape(xVec(1:4, end), 2, 2);
    bias = xVec(5:6, end);
    x1dist = -bias(1) / weight(1,1);
    x2dist = -bias(1) / weight(1, 2);
    m = (x2dist-0)/(0-x1dist);
    n = 0 - m*x1dist;
    x1(1) = min(synData(:,1));
    x1(2) = x1(1)*m+n;
    x2(1) = max(synData(:,1));
    x2(2) = x2(1)*m+n;
    aaa=plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
    x1dist = -bias(2) / weight(2,1);
    x2dist = -bias(2) / weight(2, 2);
    m = (x2dist-0)/(0-x1dist);
    n = 0 - m*x1dist;
    x1(1) = min(synData(:,1));
    x1(2) = x1(1)*m+n;
    x2(1) = max(synData(:,1));
    x2(2) = x2(1)*m+n;
    bbb=plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
    legend([aaa,bbb], 'sep1', 'sep2');
end

%% PCA projection training results
figure;
componentStr1 = {'1^{st}', '1^{st} to 2^{nd}', '1^{st} to 3^{rd}'};
for i = 1:3
    subplot(2, 3, i); imagesc(model{i+1}.gradVec'); colormap(gca, bluewhitered);  colorbar; title(['gradients on the ' componentStr1{i} ' components']); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 3, i+3); imagesc(model{i+1}.xVec'); colormap(gca, bluewhitered);  colorbar; title(['theta on the ' componentStr1{i} ' components']); ylabel('# iterations');  xlabel('weight');
end
figure;
componentStr2 = {'1^{st} to 4^{th}', '1^{st} to 5^{th}', '1^{st} to 6^{th}'};
for i = 1:3
    subplot(2, 3, i); imagesc(model{i+1+3}.gradVec'); colormap(gca, bluewhitered);  colorbar; title(['gradients on the ' componentStr2{i} ' components']); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 3, i+3); imagesc(model{i+1+3}.xVec'); colormap(gca, bluewhitered);  colorbar; title(['theta on the ' componentStr2{i} ' components']); ylabel('# iterations');  xlabel('weight');
end

%% optimization path of pca regu. (1-600 iterations), for the five different algorithms
VX = [];
costFn = @(x) supervised_dnn_cost( x, ei, train.data, train.labels, 0, true);
thetaCell{1} = model{1}.xVec(:, 1:600)';
thetaCell{2} = model{2}.xVec(:, 1:600)';
thetaCell{3} = model{3}.xVec(:, 1:600)';
thetaCell{4} = model{4}.xVec(:, 1:600)';
thetaCell{5} = model{5}.xVec(:, 1:600)';
% thetaCell{6} = model{6}.xVec';
% thetaCell{7} = model{7}.xVec';
algoName{1} = 'origianl';
algoName{2} = 'projPC 1';
algoName{3} = 'projPC 1 to 2';
algoName{4} = 'projPC 1 to 3';
algoName{5} = 'projPC 1 to 4';
% algoName{6} = 'projPC 1 to 5';
% algoName{7} = 'projPC 1 to 6';
[VX, algoName, xAryPCCell] = optimizationPathPlot( thetaCell,  algoName, costFn, [], [], VX, train.labels, 1:5);

%adding pc loading on the figure of optimization path
pcLoading = V(:,1:2)'*VX(:,1:2);
hold on; quiver(0, 0, pcLoading(1, 1), pcLoading(1, 2), 'MaxHeadSize', 0.7, 'LineWidth', 1.5);
hold on; quiver(0, 0, pcLoading(2, 1), pcLoading(2, 2), 'MaxHeadSize', 0.7, 'LineWidth', 1.5);
tmp = algoName;
algoName = cell(length(tmp)+2, 1);
cnt = 1;
for i = 1:length(algoName)
    if i <= length(tmp)
        algoName{i} = tmp{i};
    else
        algoName{i} = sprintf( 'pc %d of gradient', cnt );
        cnt = cnt + 1;        
    end
end
legend(algoName);

%plot the difference between the objective function values (v1-v2)
%v1 comes from the values based on the projected weight.
%v2 comes from the values based on the original weight.
costPCCell = cell(length(xAryPCCell), 1);
for i = 1:length(xAryPCCell)
    tmp = zeros(length(xAryPCCell{i}), 1);
    for j = 1:length(xAryPCCell{i})
        tmp(j) = costFn(xAryPCCell{i}(j,:)*VX(:, 1:2)');
    end
    costPCCell{i} = tmp;
end
figure; 
for i = 1:length(costPCCell)
    hold on; plot(costPCCell{i}-model{i}.fVec(1, 1:600)', 'LineWidth', 1.5);
end
set(gca, 'fontweight', 'bold');
title('projected objective value - real objective');
legend(algoName(2:6), 'location', 'best');

%%
%% below train differet models with pc components learned from 600 to 1000
%% pca on the 600 to 1000 iterations gradientAry
%[coeff,score,latent,tsquared,explained,mu]  = pca(gradientAry);
gradientAry = model{1}.gradVec(:, 600:1000)';
thetaAry = model{1}.xVec(:, 600:1000)';
lastGAry = gradientAry(end,:);
zeroGAry = zeros(size(lastGAry));
tmp = bsxfun( @minus, gradientAry(1:end,:), zeroGAry );
[U,S,V] = svd(tmp);
eVec = diag( S.^2 / (size(gradientAry(1:end,:), 1) - 1) );
eVec = eVec / sum(eVec);
accuEVec = cumsum(eVec);
figure; 
subplot(2,2,1); imagesc(thetaAry); colormap(gca, bluewhitered);  title('weight at each iteration'); ylabel('# iteration'); xlabel('weight'); colorbar;
subplot(2,2,2); imagesc(gradientAry);  colormap(gca, bluewhitered);  title('gradient at each iteration'); ylabel('# iteration'); xlabel('gradient'); colorbar;
subplot(2,2,3); bar(eVec); title('variance explained'); xlabel('principal component');
x = 1:size(V,2);
for i1=1:numel(eVec)
    text(x(i1),eVec(i1),num2str(eVec(i1),'%0.2e'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end
subplot(2,2,4); imagesc(V); colormap(gca, bluewhitered);  colorbar; title('principal component coeff.' ); xlabel('principal component');

%% random noise projection analysis to see if the last pc componets are the null-space of the gradient
% noise = 1*randn(9, 1);
% pNoise = (V(:,9)'*noise)*V(:,9);
scales = [5e-3, 1e-2, 1e-1, 1, 10 100];

costAry = zeros(9, 4);
figure;
for z = 1:length(scales)
    scale = scales(z);
    for i = 1:9
        pNoise = scale*V(:, i);
        [oCost,~, oPredProbs] = supervised_dnn_cost( xVec(:, end), ei, train.data, train.labels, 0, true, pcaStruc);
        [cost1, ~, predProbs1] = supervised_dnn_cost( xVec(:, end)-pNoise, ei, train.data, train.labels, 0, true, pcaStruc);
        [cost2, ~, predProbs2] = supervised_dnn_cost( xVec(:, end)+pNoise, ei, train.data, train.labels, 0, true, pcaStruc);
        costAry(i, 1) = max( abs(cost1 - oCost), abs(cost2-oCost));
    %     
%         [oCost, ~, ~] = supervised_dnn_cost( xVec(:, end), ei, test.data, test.labels, 0, true, pcaStruc);
%         [cost1, ~, ~] = supervised_dnn_cost( xVec(:, end)-pNoise, ei, test.data, test.labels, 0, true, pcaStruc);
%         [cost2, ~, ~] = supervised_dnn_cost( xVec(:, end)+pNoise, ei, test.data, test.labels, 0, true, pcaStruc);
%         costAry(i, 2) = max( abs(cost1 - oCost), abs(cost2-oCost)) / 200;
    end
    subplot(2, 3, z);
    plot(costAry(:,1), 'o-', 'linewidth', 1.5); hold on;
    title( sprintf('perturbation scale = %f, | cost difference | / sample size', scale) );
    xlabel( 'perturbation component');
%     plot(costAry(:,2), 'o-', 'linewidth', 1.5);
end

figure;
for z = 6:9
    costAry2 = zeros(1000,1);
    for i = 1:1000
        scales = rand((9-z+1),1);
        pNoise = V(:, z:9)*scales;
        [oCost,~, oPredProbs] = supervised_dnn_cost( xVec(:, end), ei, train.data, train.labels, 0, true, pcaStruc);
        [cost1, ~, predProbs1] = supervised_dnn_cost( xVec(:, end)-pNoise, ei, train.data, train.labels, 0, true, pcaStruc);
        [cost2, ~, predProbs2] = supervised_dnn_cost( xVec(:, end)+pNoise, ei, train.data, train.labels, 0, true, pcaStruc);
        costAry2(i, 1) = max( abs(cost1 - oCost), abs(cost2-oCost))/ 800;
    end
    set(gca, 'fontweight', 'bold');
    subplot(2,2,z-5);
    hist(costAry2);
    title( sprintf( 'perturbation from component %d to 9', z ) ); 
end
figure;
for z = 1:4
    costAry2 = zeros(1000,1);
    for i = 1:1000
        scales = rand(z,1);
        pNoise = V(:, 1:z)*scales;
        [oCost,~, oPredProbs] = supervised_dnn_cost( xVec(:, end), ei, train.data, train.labels, 0, true, pcaStruc);
        [cost1, ~, predProbs1] = supervised_dnn_cost( xVec(:, end)-pNoise, ei, train.data, train.labels, 0, true, pcaStruc);
        [cost2, ~, predProbs2] = supervised_dnn_cost( xVec(:, end)+pNoise, ei, train.data, train.labels, 0, true, pcaStruc);
        costAry2(i, 1) = max( abs(cost1 - oCost), abs(cost2-oCost))/ 800;
    end
    subplot(2,2,z);
    hist(costAry2);
end

%%  training a neural net model with first component's projection (batch optimization)
componentUsed = [1, 2, 3, 4, 5, 6, 7, 8, 9];
for i = 1:length(componentUsed)
    clear xVec fVec gradVec niter gnorm dx
    %select the transformed matrix
    tMat = V(:,componentUsed(1:i));
    pcaStruc.tMat = tMat;
    pcaStruc.meanGAry = zeros( size(lastGAry) );
    train.X = [ones(trainNum, 1), train.data]; 
    test.X = [ones(testNum, 1) test.data];

    rng(2018);
    stack = initialize_weights(ei);
    params = stack2params(stack);
    objFn = @(params, x, y) supervised_dnn_cost(params, ei, x, y, 0, 0, pcaStruc);
    maxiter = 1000;
    stepSize = 5e-3;
    [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent_batch(objFn, params, maxiter, stepSize, train.data, train.labels, 100, []);
    tmpStr = sprintf( '1 ~ %d', i );
    figure;plot(fVec); set(gca, 'fontweight', 'bold'); title( sprintf( 'objective function changing using pc component %s to project', tmpStr) ); xlabel('# iteration');
    model{i+1}.xVec= xVec;
    model{i+1}.fVec= fVec;
    model{i+1}.gradVec= gradVec;
    % compute accuracy on the test and train set
    [~, ~, predProbs] = supervised_dnn_cost( xVec(:, end), ei, train.data, train.labels, 0, true, pcaStruc);
    predsLabels = zeros(trainNum, 1);
    predsLabels(predProbs>0.5)=1;
    trainAcc = mean(predsLabels==train.labels);
    [~, ~, predProbs] = supervised_dnn_cost( xVec(:, end), ei, test.data, test.labels, 0, true, pcaStruc);
    predsLabels = zeros(testNum, 1);
    predsLabels(predProbs>0.5)=1;
    testAcc = mean(predsLabels==test.labels);
    fprintf('train accuracy: %f, test accuracy: %f\n', trainAcc, testAcc);
    model{i+1}.testAcc = testAcc;

    figure;scatter(train.data(train.labels==0, 1), train.data(train.labels==0,2));
    hold on;
    scatter(train.data(train.labels==1, 1),train.data(train.labels==1,2));
    set(gca, 'fontweight', 'bold'); title( sprintf( 'the hyperplanes from the hidden layer using pc component %s to project', tmpStr ) )

    weight = reshape(xVec(1:4, end), 2, 2);
    bias = xVec(5:6, end);
    x1dist = -bias(1) / weight(1,1);
    x2dist = -bias(1) / weight(1, 2);
    m = (x2dist-0)/(0-x1dist);
    n = 0 - m*x1dist;
    x1(1) = min(synData(:,1));
    x1(2) = x1(1)*m+n;
    x2(1) = max(synData(:,1));
    x2(2) = x2(1)*m+n;
    aaa=plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
    x1dist = -bias(2) / weight(2,1);
    x2dist = -bias(2) / weight(2, 2);
    m = (x2dist-0)/(0-x1dist);
    n = 0 - m*x1dist;
    x1(1) = min(synData(:,1));
    x1(2) = x1(1)*m+n;
    x2(1) = max(synData(:,1));
    x2(2) = x2(1)*m+n;
    bbb=plot(gca, [x1(1) x2(1)], [x1(2), x2(2)], 'LineWidth',2 );
    legend([aaa,bbb], 'sep1', 'sep2');
end

%% PCA projection training results
figure;
componentStr1 = {'1^{st}', '1^{st} to 2^{nd}', '1^{st} to 3^{rd}'};
for i = 1:3
    subplot(2, 3, i); imagesc(model{i+1}.gradVec'); colormap(gca, bluewhitered);  colorbar; title(['gradients on the ' componentStr1{i} ' components']); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 3, i+3); imagesc(model{i+1}.xVec'); colormap(gca, bluewhitered);  colorbar; title(['theta on the ' componentStr1{i} ' components']); ylabel('# iterations');  xlabel('weight');
end
figure;
componentStr2 = {'1^{st} to 4^{th}', '1^{st} to 5^{th}', '1^{st} to 6^{th}'};
for i = 1:3
    subplot(2, 3, i); imagesc(model{i+1+3}.gradVec'); colormap(gca, bluewhitered);  colorbar; title(['gradients on the ' componentStr2{i} ' components']); ylabel('# iterations'); xlabel('gradient');
    subplot(2, 3, i+3); imagesc(model{i+1+3}.xVec'); colormap(gca, bluewhitered);  colorbar; title(['theta on the ' componentStr2{i} ' components']); ylabel('# iterations');  xlabel('weight');
end

%% optimization path of pca regu. (600-1000 iterations)
VX = [];
costFn = @(x) supervised_dnn_cost( x, ei, train.data, train.labels, 0, true);
thetaCell{1} = model{1}.xVec(:, 600:end)';
thetaCell{2} = model{2}.xVec(:, 600:end)';
thetaCell{3} = model{3}.xVec(:, 600:end)';
thetaCell{4} = model{4}.xVec(:, 600:end)';
thetaCell{5} = model{5}.xVec(:, 600:end)';
thetaCell{6} = model{6}.xVec(:, 600:end)';
% thetaCell{7} = model{7}.xVec';
algoName{1} = 'origianl';
algoName{2} = 'projPC 1';
algoName{3} = 'projPC 1 to 2';
algoName{4} = 'projPC 1 to 3';
algoName{5} = 'projPC 1 to 4';
algoName{6} = 'projPC 1 to 5';
% algoName{7} = 'projPC 1 to 6';
[VX, algoName, xAryPCCell] = optimizationPathPlot( thetaCell,  algoName, costFn, [], [], VX, train.labels, 1:6);

% hold on; quiver(0, 0, -0.3366, -0.4772, 'MaxHeadSize', 0.7);
% legend( '', 'original', 'projPC1', 'projPC1PC2', 'lastGradient');
% export_fig contour_plot_centered_last_gradient.pdf -transparent
pcLoading = V(:,1:2)'*VX(:,1:2);
hold on; quiver(0, 0, pcLoading(1, 1), pcLoading(1, 2), 'MaxHeadSize', 0.7, 'LineWidth', 1.5);
hold on; quiver(0, 0, pcLoading(2, 1), pcLoading(2, 2), 'MaxHeadSize', 0.7, 'LineWidth', 1.5);
tmp = algoName;
algoName = cell(length(tmp)+2, 1);
cnt = 1;
for i = 1:length(algoName)
    if i <= length(tmp)
        algoName{i} = tmp{i};
    else
        algoName{i} = sprintf( 'pc %d of gradient', cnt );
        cnt = cnt + 1;        
    end
end
legend(algoName);

costPCCell = cell(length(xAryPCCell), 1);
for i = 1:length(xAryPCCell)
    tmp = zeros(length(xAryPCCell{i}), 1);
    for j = 1:length(xAryPCCell{i})
        tmp(j) = costFn(xAryPCCell{i}(j,:)*VX(:, 1:2)');
    end
    costPCCell{i} = tmp;
end
figure; 
for i = 1:length(costPCCell)
    hold on; plot(costPCCell{i}-model{i}.fVec(1, 600:end)', 'LineWidth', 1.5);
end
set(gca, 'fontweight', 'bold');
title('projected objective value - real objective');
legend(algoName(2:7), 'location', 'best');
