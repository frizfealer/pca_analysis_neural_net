function [ cost, grad, pred_prob, hidden_outs] = supervised_dnn_cost( theta, ei, data, labels, lambda, pred_only, pcaStruc)
%supervised_dnn_cost neural net cost
%input:
%   theta: model parameters
%   ei: network data structurem, has fields:
%       input_dim
%       output_dim
%       ei.layer_sizes, a vector includes each dimension of each layer,
%       except the input layer
%   data: data array
%   labels: label for the data
%   lambda: l2-regularization weight
%   pred_only: a flag to indicate this function to do forward prop. only to
%   decrease computation.
%   pcaStruc: a structure for pca projection on gradient

%% default values
po = false;
if exist('pred_only','var')
  po = pred_only;
end;

%% reshape into network
stack = params2stack(theta, ei);
numHidden = numel(ei.layer_sizes) - 1;
hAct = cell(numHidden+1, 1);
gradStack = stack; %though using W, b, it records gradients
%% forward prop
for i = 1:length(stack)
    if i > 1
        prevRep = hAct{i-1};
    else
        prevRep = data;
    end
    cStack = stack{i};
    hAct{i} = sigmoid( bsxfun(@plus, prevRep*cStack.W',  cStack.b' ) );
end
pred_prob = hAct{end};
hidden_outs = hAct(1:end-1);
%% return here if only predictions desired.
if po
    cost = -labels'*log(hAct{end} + 1e-16) - (1-labels)'*log(1-hAct{end} + 1e-16) + lambda*0.5*sum(theta.^2);%cost
    ceCost = -1; wCost = -1; numCorrect = -1;
    grad = [];
    return;
end;

%% compute cross-entropy cost
cost = -labels'*log(hAct{end} + 1e-16) - (1-labels)'*log(1-hAct{end} + 1e-16) + lambda*0.5*sum(theta.^2);%cost
%% compute gradients using backpropagation
error = cell(numHidden+1, 1);
error{end} = -( labels./(hAct{end}+1e-16) - (1-labels)./(1-hAct{end}+1e-16) ).*hAct{end}.*(1-hAct{end}); %at the output layer
for i = numHidden:-1:1
    error{i} = (error{i+1}*stack{i+1}.W).*hAct{i}.*(1-hAct{i});
end
for i = 1:length(stack)
    if i > 1
        prevRep = hAct{i-1};
    else
        prevRep = data;
    end
    gradStack{i}.W = error{i}'*prevRep + lambda*stack{i}.W;
    gradStack{i}.b = sum( error{i} )';
end
%% reshape gradients into vector
[grad] = stack2params(gradStack);

if ~isempty(pcaStruc)
    grad = grad - pcaStruc.meanGAry';
    tmp = (pcaStruc.tMat'*grad);
    grad = pcaStruc.tMat*tmp;
    grad = grad+ pcaStruc.meanGAry';
end

end
