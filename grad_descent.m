function [xVec,fVec, gradVec, niter,gnorm,dx] = grad_descent(objFn, x0, maxiter, stepSize)
% grad_descent.m demonstrates how the gradient descent method can be used
% to solve a simple unconstrained optimization problem. Taking large step
% sizes can lead to algorithm instability. The variable alpha below
% specifies the fixed step size. Increasing alpha above 0.32 results in
% instability of the algorithm. An alternative approach would involve a
% variable step size determined through line search.
%
% This example was used originally for an optimization demonstration in ME
% 149, Engineering System Design Optimization, a graduate course taught at
% Tufts University in the Mechanical Engineering Department. A
% corresponding video is available at:
% 
% http://www.youtube.com/watch?v=cY1YGQQbrpQ
%
% Author: James T. Allison, Assistant Professor, University of Illinois at
% Urbana-Champaign
% Date: 3/4/12

% termination tolerance
tol = 1e-6;

% maximum number of allowed iterations
%maxiter = 100;

% minimum allowed perturbation
dxmin = 1e-6;

% step size ( 0.33 causes instability, 0.2 quite accurate)
alpha = stepSize;

% initialize gradient norm, optimization vector, iteration counter, perturbation
gnorm = inf; x = x0; niter = 0; dx = inf;
xVec = zeros(length(x0), maxiter);
gradVec = zeros(length(x0), maxiter);
fVec = zeros(1, maxiter);
% gradient descent algorithm:
tolCnt = 0;
while and(gnorm>=tol, and(niter <= maxiter, dx >= dxmin))
    % calculate gradient:
    [f, g] = objFn(x);
    gnorm = norm(g);
    %test a step
    xtest= x - alpha*g;
    ftest = objFn(xtest);
%     if ftest > f
%         tolCnt = tolCnt + 1;
%     else
%         tolCnt = 0;
%     end
%     if tolCnt > 10
%         break
%     end
    while ftest > f
        alpha = alpha*0.8;
        xtest= x - alpha*g;
        ftest = objFn(xtest);   
        fprintf( 'niter = %d, updated alpha = %f\n', niter, alpha);
    end
%     if ftest > f
%         break
%     end
    % take step:
    xnew = x - alpha*g;
    % check step
    if ~isfinite(xnew)
        display(['Number of iterations: ' num2str(niter)])
        error('x is inf or NaN')
    end
    % update termination metrics
    niter = niter + 1;
    dx = norm(xnew-x);
    x = xnew;
    xVec(:, niter) = x;
    gradVec(:, niter) = g;
    fVec(niter) = f;
end
niter = niter - 1;
fVec = fVec(1:niter);
xVec = xVec(:,1:niter);
gradVec = gradVec(:, 1:niter);
end